import { Component, OnInit } from '@angular/core';
import { Sale } from '../sale';
import { SaleService } from '../sale.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  sales: Sale[] = [];

  constructor(private saleService: SaleService) { }

  ngOnInit() {
    this.getSales();
  }

  getSales(): void {
    this.saleService.getSales()
      .subscribe(sales => this.sales = sales.slice(1, 5));
  }

}
