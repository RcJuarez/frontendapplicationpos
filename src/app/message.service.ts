import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  messages: string[] = [];

  add(mesage: string) {
    this.messages.push(mesage);
  }

  clear() {
    this.messages = [];
  }

  constructor() { }
}
