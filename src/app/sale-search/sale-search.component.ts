import { Component, OnInit } from '@angular/core';

import { Observable, Subject } from 'rxjs';

import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';

import { Sale } from '../sale';
import { SaleService } from '../sale.service';
import { Product } from '../products/product';


@Component({
  selector: 'app-sale-search',
  templateUrl: './sale-search.component.html',
  styleUrls: ['./sale-search.component.css']
})

export class SaleSearchComponent implements OnInit {
  sales$: Observable<Sale[]>;
  products$: Observable<Product[]>;

  private searchTerms = new Subject<string>();

  constructor(private saleService: SaleService) {}

    // Push a search term into the observable stream.
    search(term: string): void {
      this.searchTerms.next(term);
    }

  ngOnInit(): void {
    this.products$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),

      // ignore new term if same as previous term
      distinctUntilChanged(),

      // switch to new search observable each time the term changes
      switchMap((term: string) => this.saleService.searchSales(term)),
    );
  }

}