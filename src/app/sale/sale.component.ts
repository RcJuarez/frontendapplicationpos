import { Component, OnInit } from '@angular/core';

import { Sale } from '../sale';
import { SaleService } from '../sale.service';
import { Product } from '../products/product';
import { ProductsComponent } from '../products/products.component';
import { ProductService } from '../Services/Product.service';

@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.css']
})

export class SaleComponent implements OnInit {

  sales: Sale[];
  products: Product[];

  constructor(
    private saleService: SaleService,
    private productServicce: ProductService
    ) { }

  ngOnInit() {
    //this.getSales();
    this.getProducts();
  }
/*
  getSales(): void {
    this.saleService.getSales()
        .subscribe(sales => this.sales = sales);
  }
*/
  getProducts(): void {
    this.productServicce.getProductsDB()
    .subscribe( prod => this.products = prod);
  }

  add(codeProduct: string): void {
    codeProduct = codeProduct.trim();
    if (!codeProduct) { return; }
    this.saleService.addSale({ codeProduct } as Sale)
      .subscribe(sale => {
        this.sales.push(sale);
      });
  }

  findProduct(codeProdct:string): void {
    this.productServicce.getProductByCode(codeProdct);
  }

  delete(sale: Sale): void {
    this.sales = this.sales.filter(h => h !== sale);
    this.saleService.deleteSale(sale).subscribe();
  }

}
