import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Sale } from '../sale';
import { SaleService }  from '../sale.service';

@Component({
  selector: 'app-sale-detail',
  templateUrl: './sale-detail.component.html',
  styleUrls: ['./sale-detail.component.css']
})
export class SaleDetailComponent implements OnInit {

  @Input() sale: Sale;

  constructor(
    private route: ActivatedRoute,
    private saleService: SaleService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getSale();
  }

  getSale(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.saleService.getSale(id)
      .subscribe(sale => this.sale = sale);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.saleService.updateSale(this.sale)
      .subscribe(() => this.goBack());
  }
}
