export class Product {
    code: string;
    bar_code: string;
    name: string;
    description: string;
    common_names: string;
    category_id: number;
  }