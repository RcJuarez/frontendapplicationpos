import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Product } from './../products/product';

@Injectable({ providedIn: 'root' })

export class ProductService {

  private productsUrl =  'http://localhost:8081/products'  // URL to web api
  private currentEuroRates: any = null;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService
    ) { }

/** GET sales from the server */
getProducts (): Observable<Product[]> {
  return this.http.get<Product[]>(this.productsUrl);
}

/*
getProductsDB(): Observable<Product[]> {
  return this.http.get(environment.urlProduct).subscribe( apiData => (this.currentEuroRates))
  //.subscribe(apiData => (this.currentEuroRates = apiData));
}
*/
/*
getProductsDB() {
  const url = environment.urlProduct;
  this.currentEuroRates = this.http.get(url);
}
*/

/** GET products from the server */
getProductsDB (): Observable<Product[]> {
  /*
  return this.http.get<Product[]>(environment.urlProduct)
    .pipe(
      tap(_ => this.log('fetched products')),
      catchError(this.handleError<Product[]>('getProductsDB', []))
    );
    */
   return this.http.get<Product[]>(this.productsUrl);
}

/** GET product by id. Will 404 if id not found */
getProduct(id: number): Observable<Product> {
  const url = `${this.productsUrl}/${id}`;
  return this.http.get<Product>(url);
}

getProductByCode(code: string): Observable<Product[]> {
  const url = `${this.productsUrl}`;
  return this.http.get<Product[]>(`${url}/?cadena=${code}`);
}

  /* GET sales whose name contains search term */
  searchSales(term: string): Observable<Product[]> {
    if (!term.trim()) {
      // if not search term, return empty hero array.
      return of([]);
    }
    return this.http.get<Product[]>(`${this.productsUrl}/?name=${term}`);
  }

   //////// Save methods //////////
  /** POST: add a new product to the server */
  addSale (product: Product): Observable<Product> {
    return this.http.post<Product>(this.productsUrl, product, this.httpOptions);
  }

    /** DELETE: delete the product from the server */
    deleteSale (product: Product | number): Observable<Product> {
      const id = typeof product === 'number' ? product : product.code;
      const url = `${this.productsUrl}/${id}`;
  
      return this.http.delete<Product>(url, this.httpOptions);
    }

      /** PUT: update the product on the server */
  updateSale (product: Product): Observable<any> {
    return this.http.put(this.productsUrl, product, this.httpOptions);
  }
    
  private log(message: string) {
    this.messageService.add(`SaleService: ${message}`);
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
