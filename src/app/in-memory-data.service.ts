import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Sale } from './sale';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  createDb() {
    const sales = [
      { id: 11, codeProduct: 'Dr Nice' },
      { id: 12, codeProduct: 'Narco' },
      { id: 13, codeProduct: 'Bombasto' },
      { id: 14, codeProduct: 'Celeritas' },
      { id: 15, codeProduct: 'Magneta' },
      { id: 16, codeProduct: 'RubberMan' },
      { id: 17, codeProduct: 'Dynama' },
      { id: 18, codeProduct: 'Dr IQ' },
      { id: 19, codeProduct: 'Magma' },
      { id: 20, codeProduct: 'Tornado' }
    ];
    return {sales};
  }

  // Overrides the genId method to ensure that a hero always has an id.
  // If the heroes array is empty,
  // the method below returns the initial number (11).
  // if the heroes array is not empty, the method below returns the highest
  // hero id + 1.
  genId(sales: Sale[]): number {
    return sales.length > 0 ? Math.max(...sales.map(sale => sale.id)) + 1 : 11;
  }

  constructor() { }
}
